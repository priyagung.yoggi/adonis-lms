import Enrollment from 'app/Models/Enrollment'

import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'

export default class CourseByCycle extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @hasMany(() => Enrollment)
  public enrollment: HasMany<typeof Enrollment>
  
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
