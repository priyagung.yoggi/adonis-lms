import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class AuthController {
    public async register({request, response }: HttpContextContract) {
        const body = request.body()
        
        const user = await User.create(body)
        return response.created(user);
        // return user;
    }
    public async login({auth, request, response}: HttpContextContract){
        const email = request.input("email")
        const password = request.input("password")
        try {
            const token = await auth.use('api').attempt(email,password,{
                expiresIn: '24hours'
            })
            return token.toJSON()
        } catch (error) {
            return response.unauthorized('invalid credentials')
        }
    }

    public async index({}: HttpContextContract) {
        return User.all()
    }
}
