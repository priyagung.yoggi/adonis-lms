import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'enrollments'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table
        .integer('course_by_cycle_id')
        .unsigned()
        .references('course_by_cycles.id')
        .onDelete('CASCADE')
      table
        .integer('course_id')
        .unsigned()
        .references('course_by_cycles.id')
        .onDelete('CASCADE')

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
