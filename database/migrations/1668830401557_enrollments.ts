import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'enrollments'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      // table
      //   .integer('course_by_cycle_id')
      //   .unsigned()
      //   .references('course_by_cycles.id')
      //   .onDelete('CASCADE')
      // table
      //   .integer('course_id')
      //   .unsigned()
      //   .references('course_by_cycles.id')
      //   .onDelete('CASCADE')
      table
        .integer('user_id')
        .unsigned()
        .references('users.id')
        .onDelete('CASCADE')
      table.boolean('canceled')
      table.string('cancelationReason')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
